-- Disclaimer: This is an unfinished piece of work --

This is a simple calculator scripts built with ReactJS (ES6)

To install, clone this repo

Use "npm install" in the root directory.

Once installed, use "npm start" to run a local version.

You can use "npm run build" to generate a prod-ready version thanks to CRA's build tools.
