import React, { Component } from 'react';
import './App.css';

class App extends Component {

    constructor() {
        super();
        this.calculator = {
            total: 0,
            typing: 0,
            calc: null,
            prev: 0,
            history: []
        }
    }

    clickNumber(num) {
        this.calculator.typing = String(this.calculator.typing + num);
        this.calculator.history.push(num);
        this.renderCalculate();
    }

    clickCalculate(type) {
        if(this.calculator.calc && this.calculator.prev)
        {
            if(this.calculator.calc === "+")
            {
                this.calculator.total = parseInt(this.calculator.total, 10) + parseInt(this.calculator.typing, 10);
            }
            else if(this.calculator.calc === "-")
            {
                this.calculator.total = parseInt(this.calculator.total, 10) - parseInt(this.calculator.typing, 10);
            }
            else if(this.calculator.calc === "*")
            {
                this.calculator.total = parseInt(this.calculator.total, 10) * parseInt(this.calculator.typing, 10);
            }
            else if(this.calculator.calc === "/")
            {
                this.calculator.total = parseInt(this.calculator.total, 10) / parseInt(this.calculator.typing, 10);
            }
            else if(this.calculator.calc === "=")
            {
                this.calculator.total = parseInt(this.calculator.total, 10);
            }
        }
        else
        {
            this.calculator.total = this.calculator.typing;
        }

        // UPDATE CALC HISTORY
        this.calculator.history.push(type);

        // RESET CALCULATOR & RERENDER VALUES
        this.calculator.calc = type;
        this.calculator.prev = this.calculator.typing;
        this.calculator.typing = 0;
        this.renderCalculate();
    }

    renderCalculate() {
        var history = "";
        this.calculator.history.forEach(calc => {
            history = String(history + calc);
        });
        console.log(history);
        this.forceUpdate();
    }

    clearCalculator() {
        this.calculator = {
            total: 0,
            typing: 0,
            calc: null,
            prev: 0,
            history: []
        }
        this.forceUpdate();
    }

    render() {
        console.log(this.calculator.history);
        return (
            <div>
                <div className="container">
                    <div className="calc-body">
                        <div className="calc-screen">
                            <div className="calc-operation">{this.calculator.total}</div>
                            <div className="calc-typed">{this.calculator.typing}<span className="blink-me">_</span></div>
                        </div>
                        <div className="calc-button-row">
                            <div className="button c" onClick={() => this.clearCalculator("+")}>C</div>
                            <div className="button l" onClick={() => this.clickCalculate("/")}>/</div>
                        </div>
                        <div className="calc-button-row">
                            <div className="button" onClick={() => this.clickNumber(7)}>7</div>
                            <div className="button" onClick={() => this.clickNumber(8)}>8</div>
                            <div className="button" onClick={() => this.clickNumber(9)}>9</div>
                            <div className="button l" onClick={() => this.clickCalculate("x")}>x</div>
                        </div>
                        <div className="calc-button-row">
                            <div className="button" onClick={() => this.clickNumber(4)}>4</div>
                            <div className="button" onClick={() => this.clickNumber(5)}>5</div>
                            <div className="button" onClick={() => this.clickNumber(6)}>6</div>
                            <div className="button l" onClick={() => this.clickCalculate("-")}>−</div>
                        </div>
                        <div className="calc-button-row">
                            <div className="button" onClick={() => this.clickNumber(1)}>1</div>
                            <div className="button" onClick={() => this.clickNumber(2)}>2</div>
                            <div className="button" onClick={() => this.clickNumber(3)}>3</div>
                            <div className="button l" onClick={() => this.clickCalculate("+")}>+</div>
                        </div>
                        <div className="calc-button-row">
                            <div className="button">.</div>
                            <div className="button">0</div>
                            <div className="button">></div>
                            <div className="button l" onClick={() => this.clickCalculate("=")}>=</div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

}

export default App;
